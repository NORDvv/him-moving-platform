int driverPUL = 12;
int driverDIR = 11;
int driverENA = 10;

int pulseDelay = 50000;
boolean setDirection = LOW;
int Position = 0;

boolean newCommand = false;

int command;
void setup() {
  pinMode (driverPUL, OUTPUT);
  pinMode (driverDIR, OUTPUT);
  Serial.begin(9600);
}
 
void loop() {
  if ( Serial.available() > 0) {
    command = Serial.parseInt();
    newCommand = true;
  }
  if (newCommand) {

    move(command);
    newCommand = false;
  }
}

void move(int command) {
  if (command == 1) {
      counterclockwise();
      Position++;
      Serial.print(Position);
      Serial.print("    Good\n");
  } else if (command == -1) {
      clockwise();
      Position--;
      Serial.print(Position);
      Serial.print("    Good\n");
  } else if (command == 24) { 
      Serial.print(Position);
      Serial.print("    Reset\n");
  } else {
    Serial.print("0    Fail");
  }
}

void counterclockwise(){
    setDirection = LOW;
    for (int i = 0; i < 200; i++)
    {
        digitalWrite(driverDIR,setDirection);
        digitalWrite(driverENA,HIGH);
        digitalWrite(driverPUL,HIGH);
        delayMicroseconds(pulseDelay);
        digitalWrite(driverPUL,LOW);
        delayMicroseconds(pulseDelay);
    }
}

void clockwise(){
    setDirection = HIGH;
    for (int i = 0; i < 200; i++)
    {
        digitalWrite(driverDIR,setDirection);
        digitalWrite(driverENA,HIGH);
        digitalWrite(driverPUL,HIGH);
        delayMicroseconds(pulseDelay);
        digitalWrite(driverPUL,LOW);
        delayMicroseconds(pulseDelay);
    }
}
