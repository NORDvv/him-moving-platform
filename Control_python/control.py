from PyQt5 import QtWidgets, uic
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
from PyQt5.QtCore import QIODevice

import time
import random

app = QtWidgets.QApplication([])
ui = uic.loadUi("interface.ui")
ui.setWindowTitle("Platform Control")


serial = QSerialPort()
serial.setBaudRate(9600)
portList = []
ports = QSerialPortInfo().availablePorts()
for port in ports:
    portList.append(port.portName())
ui.comPorts.addItems(portList)



def onRead():
    if not serial.canReadLine(): return
    rx = serial.readLine()
    rxs = str(rx, 'utf-8').strip()
    ui.textBrowser.append(rxs)
    postion = int(rxs[0:3].strip())
    ui.realPossitionBar.setValue(postion)
    ui.PlatformPositionDisplay.display(postion)
    ui.dial.setValue(postion)
    return postion

def outputNo():
    random.seed()
    chance = random.randint(0,100)
    if (chance == 0):
        ui.textBrowser.append("Problem solved!\nJust kidding- spamming the button won't help. C'mon")
    else:
        ui.textBrowser.append("No!")

def Reset():
    reset_text = "If nothing works:\n"\
        "1. Reset Arduino (push and hold the only button on it)\n"\
        "2. Check (physically) if the screw is still holding a platform (open a microscope and look at it. Poke the platform)\n"\
        "3. Restart the current program after closing the microscope and connecting the Arduino to the computer\n"
    ui.textBrowser.append(reset_text)

def HelpTextOut():
    help_text = "Steps to execute in given order:\n" \
                "1. Select one of given ports (On the left- COM1,COM2 ...)\n"\
                "1.5 Wait for 5 seconds allowing system to load properly\n"\
                "2. Click 'Port is Open' - the corresponding message should apperar\n"\
                "3. Control platform using the dial - once clicked, it will automatically move\n to the correct posstion\n"\
                "4. Arduino will provide feed back in the form of '<position> <status>'. It should be a numbers and 'Good'\n"\
                "5. If 'Fail' is displayed- got to the 'Reset'section\n"
    ui.textBrowser.append(help_text)

def ClearText():
    ui.textBrowser.clear()

def outputTroubleshoot():
    trouble_text = " - If <number> <status> doesn't show up - check if port is connected\n"\
        " - If dile is jumping back and forth after you click it - it works as intended\n"\
        " - If Arduino was disconnected from powersupply - manually rotate the screw to the closes position possible and reset\n"\
        " - If something else bad is happening - you have found a bug, which I haven't noticed. Congratulations! Report it via provided contacts in documetation\n"
    ui.textBrowser.append(trouble_text)


def portSwitch(switchStatus):
    if switchStatus == 2:
        serial.setPortName(ui.comPorts.currentText())
        serial.open(QIODevice.ReadWrite)
        ui.textBrowser.append("Port Open")
    else:
        ui.textBrowser.append("Port Closed")
        serial.close()




def motorControl(newReading):
    platform_position = ui.PlatformPositionDisplay.intValue()
    ui.PlatformPositionDisplay.display(newReading)
    if platform_position < newReading:
        serial.write('1'.encode())
        ui.textBrowser.append("Forward!")
            
    elif platform_position > newReading:
        serial.write('-1'.encode())
        ui.textBrowser.append("Backwards!")
            


ui.ButtonHelp.clicked.connect(HelpTextOut)
ui.ButtonClear.clicked.connect(ClearText)
ui.ButtonReset.clicked.connect(Reset)
ui.ButtonNo.clicked.connect(outputNo)
ui.ButtonTroubleshoot.clicked.connect(outputTroubleshoot)

ui.PortOpenCheckBox.stateChanged.connect(portSwitch)
ui.dial.valueChanged.connect(motorControl)

serial.readyRead.connect(onRead)
ui.show()
app.exec()