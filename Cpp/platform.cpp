#include "platform.h"

void readLogFileData(int &forward_clicks_counter, int &totla_clicks) {
    fstream logFile;
    logFile.open("logFile.csv");
    
    string line;
    getline(logFile,line);//To remove the heading
    
    if (line.length() == 0) {
        errorMessageLogWrite("Log file is lost, empty or something else is wrong");
        forward_clicks_counter = 0;
        totla_clicks = 0;
        return;
    }

    int comaIndex;

    getline(logFile,line); 
    comaIndex = line.find(',');
    forward_clicks_counter = stoi(line.substr(0,comaIndex));
    
    line.erase(0,comaIndex+1);
    comaIndex = line.find('\n');
    totla_clicks = stoi(line.substr(0,comaIndex));
    
    logFile.close();

    checkForMaxClicks(totla_clicks);
    if (forward_clicks_counter > max_clicks || forward_clicks_counter < min_clicks) {
        cout << "ERROR: INADEQUATE READINGS! RESET IS STRONGLY RECOMENDED! PROCEED AT OWN RISK!" << endl;
        errorMessageLogWrite("Inadequate log reading.");
    }
}

void checkIfReset() {
    cout << "Would you like to perform a reset on the platform? y/n/?  ";
    char resetAnswer = '?';
    
    while (resetAnswer != 'n') {
        cin >> resetAnswer;
        if (resetAnswer == 'y') {
            cout << "Resetting" << endl;// TODO Perform a reset, if needed
            break;
        } else if (resetAnswer != 'n') {
            cout << "For that opening the vacuum chamber and manually rotating the platform" <<
            " back to initial position would be required" << endl;
            cout << "\nWould you like to perform a reset on the platform? y/n/?  ";
        }
    }
    cout << "\n\n";
}

void errorMessageLogWrite(string errorMessage) {
    ofstream myfile;
    myfile.open ("errors_log.txt",ios::ate | ios::app); // TODO cahnge to a hidden file
    myfile << "<---------------------------->\n";
    myfile << "ERROR MESSAGE: " << errorMessage << endl;

    time_t now = time(0); // current date/time based on current system
    char* dt = ctime(&now);// convert now to string form
    myfile << "The local date and time is: " << dt << endl;
    tm *gmtm = gmtime(&now);// convert now to tm struct for UTC
    dt = asctime(gmtm);
    myfile << "The UTC date and time is:"<< dt << endl;
    myfile << "<---------------------------->\n";

    myfile.close();
}

void checkForMaxClicks(int clicks) {
    if (clicks > 500) {
        cout << "WARNING: OPERATING FOR TOO LONG WITH NO RESET! PROCEED AT OWN RISK!" << endl;
        errorMessageLogWrite("Further operation without reset may be harmful!");
    }
}

bool sendCommands() {
    char user_input;
    /*cout << "Enter the command: 'F' to move forward, 'B' to move bacwards,\n" <<
            "'I' for increasing number of rotations/command, 'D' for devreasing the number of rotations/comand\n" <<
            "Press 'Q' to exit the program (do not close the window just yet)" 
            << endl;*/
    cout << "Next command (one letter): ";
    cin >> user_input;

    //All fall-through in case statements are intentional
    switch(user_input) {
        case 'q':
        case 'Q':
            cout << "Exiting the program loop" << endl;
            return false;
        case 'f':
        case 'F':
            cout << "-> Moving forward" << endl;
            sendCharToArduino('F');
            break;
        case 'b':
        case 'B':
            cout << "<- Moving backwards" << endl;
            sendCharToArduino('F');
            break;
        case 'i':
        case 'I':
            cout << "/\\ Increasing steps per click" << endl;
            sendCharToArduino('F');
            break;
        case 'd':
        case 'D':
            cout << "\\/ Decreasing steps per click" << endl;
            sendCharToArduino('F');
            break;
        default :
            cout << "Valid Input:" << endl;
            cout << "F - forward\t\t" <<
            "B - backwards\n" << 
            "I - increase rev/click\t" << 
            "D - decrease rev/click\n" << 
            "Q - quit the programm\n";
    }
    
    return true;
}



void sendCharToArduino(char msg) {
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = &msg;
    //char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return;
    }
   
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
       
    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) 
    {
        printf("\nInvalid address/ Address not supported \n");
        return;
    }
   
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return;
    }
    send(sock , hello , 1 , 0 );
    printf("Message sent\n");
    //valread = read( sock , buffer, 1024);
    //printf("%s\n",buffer );
}

void writeLogFileData(int &forward_clicks_counter, int &totla_clicks) {
    //TODO finish
}