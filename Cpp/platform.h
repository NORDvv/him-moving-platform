#ifndef PLATFORM_CONTROL_H
#define PLATFORM_CONTROL

#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <ctime>

#include <stdio.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <pthread.h>
#include <netdb.h>
#include <unistd.h>	
#include <signal.h>

#define PORT 0043 // /dev/ttyACM0



using namespace std;

const int max_clicks = 25;
const int min_clicks = 0;

void checkIfReset();
void readLogFileData(int &forward_clicks_counter, int &totla_clicks);
void errorMessageLogWrite(string errorMessage);
void resetPlatform();
void checkForMaxClicks(int clicks);
bool sendCommands();
void sendCharToArduino(char msg);
void writeLogFileData(int &forward_clicks_counter, int &totla_clicks);


#endif