# HIM moving platform

A remotely controlled platform for He Ion. Project consist of a program for a PC through which an operator can control the platform, and a program for Arduino to perform manipulation with the electric motor of the platform.

To run the program some additonal software is required:
 - Python 3
 - Add Python to PATH
 - PyQt5 (open the terminal and type: install PyQt5)
